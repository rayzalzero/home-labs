# homelabs

## Getting started

To make it easy for you to get started, here's a list of services.

## List of services
- adguardhome
- traefik
- portainer
- uptime-kuma
- jellyfin
- watchtower
- transmission
- filebrowser
- homer
- homeassistant
- jackett
- radarr
- sonarr
- zerotier

## Installation
Create an environment by copying file `.env.example` to `.env` and adjust the value as needed.

## Usage
Run command docker compose to build services
```
docker compose up -d
```

To run a chosen container from the list above 

```
docker compose up -d {container-name}
```
The container name {container-name} is the same as its service name